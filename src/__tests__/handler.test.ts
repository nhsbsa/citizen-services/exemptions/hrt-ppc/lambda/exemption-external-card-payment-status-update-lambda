import { SQSEvent } from "aws-lambda";
import { handler } from "../index";
import { processRecord } from "../process-sqs-event-record";
import mockEventSingleRecord from "../../events/event-single-record.json";
import mockEventMultipleRecords from "../../events/event-multiple-records.json";
import { logger } from "@nhsbsa/hrt-ppc-npm-logging";

jest.mock("../process-sqs-event-record");

let event: SQSEvent;
let loggerSpy: jest.SpyInstance;
const mockProcessRecord = processRecord as jest.MockedFunction<
  typeof processRecord
>;

beforeEach(() => {
  jest.useFakeTimers().setSystemTime(new Date("2020-01-01"));
  event = JSON.parse(JSON.stringify(mockEventSingleRecord));
  mockProcessRecord.mockClear();
  mockProcessRecord.mockResolvedValue();
  loggerSpy = jest.spyOn(logger, "error");
});

describe("handler", () => {
  it("should log the error when failed to process a single record", async () => {
    const error = Error("Internal Server Error");
    mockProcessRecord.mockRejectedValueOnce(error);

    await expect(() =>
      handler(event),
    ).rejects.toThrowErrorMatchingInlineSnapshot(
      `"[1] out of total [1] records failed to process"`,
    );
    expect(loggerSpy.mock.calls[0][0]).toStrictEqual(
      `Internal Server Error: ${error.stack}`,
    );
    expect(mockProcessRecord).toBeCalledTimes(1);
  });

  it("should log the error when failed to process the 2nd record, batch size of 3", async () => {
    event = JSON.parse(JSON.stringify(mockEventMultipleRecords));
    const error = Error("Internal Server Error");
    mockProcessRecord
      .mockResolvedValueOnce()
      .mockRejectedValueOnce(error)
      .mockResolvedValueOnce();

    await expect(() =>
      handler(event),
    ).rejects.toThrowErrorMatchingInlineSnapshot(
      `"[1] out of total [3] records failed to process"`,
    );
    expect(loggerSpy.mock.calls[0][0]).toStrictEqual(
      `Internal Server Error: ${error.stack}`,
    );
    expect(mockProcessRecord).toBeCalledTimes(3);
  });

  it("should handles event successfully without any error", async () => {
    await expect(handler(event)).resolves.not.toThrowError();
    expect(mockProcessRecord).toBeCalledTimes(1);
    expect(loggerSpy).toHaveBeenCalledTimes(0);
  });
});
