import { processRecord } from "../process-sqs-event-record";
import { mockSQSRecord, mockPaymentRecord } from "../__mocks__/mockData";
import { randomUUID } from "crypto";
import * as payment from "../api-utils/payment";

jest.mock("crypto");

let findByTransactionIdSpy: jest.SpyInstance;
let patchPaymentRecordSpy: jest.SpyInstance;
const mockRandomUUID = randomUUID as jest.MockedFunction<typeof randomUUID>;

let sqsRecord;
beforeEach(() => {
  sqsRecord = { ...mockSQSRecord };
  findByTransactionIdSpy = jest
    .spyOn(payment, "findByTransactionId")
    .mockResolvedValue(mockPaymentRecord);

  patchPaymentRecordSpy = jest
    .spyOn(payment, "patchPaymentRecord")
    .mockResolvedValue();
  mockRandomUUID.mockReturnValue("a580305a-dae9-46a4-a420-ddee91054d7a");
});
afterEach(() => {
  jest.resetAllMocks();
  jest.restoreAllMocks();
});

describe("processRecord()", () => {
  it("should throw the error from patch payment api", async () => {
    jest
      .spyOn(payment, "patchPaymentRecord")
      .mockRejectedValue(new Error("Api error"));
    await expect(() => processRecord(sqsRecord)).rejects.toThrow(
      new Error("Api error"),
    );
  });

  it("should process the sqs record successfully without any error", async () => {
    await expect(processRecord(sqsRecord)).resolves.not.toThrowError();
    expect(findByTransactionIdSpy).toBeCalledTimes(1);
    expect(patchPaymentRecordSpy).toBeCalledTimes(1);

    expect(findByTransactionIdSpy.mock.calls[0][0]).toMatchInlineSnapshot(`
      {
        "headers": {
          "channel": "BATCH",
          "correlation-id": "a580305a-dae9-46a4-a420-ddee91054d7a",
          "user-id": "CPS_EVENT",
        },
        "transactionId": "68B68A05E2B9C9971C0A",
      }
    `);
    expect(patchPaymentRecordSpy.mock.calls[0][0]).toMatchInlineSnapshot(`
      {
        "data": {
          "status": "SUCCESS",
        },
        "headers": {
          "channel": "BATCH",
          "correlation-id": "a580305a-dae9-46a4-a420-ddee91054d7a",
          "user-id": "CPS_EVENT",
        },
        "paymentId": "ac1bed7d-8597-156c-8185-97562f3c0000",
      }
    `);
  });

  it("should not call patch apis when no payment record found", async () => {
    findByTransactionIdSpy.mockResolvedValue({
      transactionId: "68B68A05E2B9C9971C0A",
    });
    await expect(processRecord(sqsRecord)).rejects.toThrowError();
    expect(findByTransactionIdSpy).toBeCalledTimes(1);
    expect(patchPaymentRecordSpy).toBeCalledTimes(0);
    expect(findByTransactionIdSpy.mock.calls[0][0]).toMatchInlineSnapshot(`
      {
        "headers": {
          "channel": "BATCH",
          "correlation-id": "a580305a-dae9-46a4-a420-ddee91054d7a",
          "user-id": "CPS_EVENT",
        },
        "transactionId": "68B68A05E2B9C9971C0A",
      }
    `);
  });

  it("should not call patch apis when payment record found has no paymentId", async () => {
    findByTransactionIdSpy.mockResolvedValue({
      transactionId: "68B68A05E2B9C9971C0A",
    });
    await expect(processRecord(sqsRecord)).rejects.toThrowError();
    expect(findByTransactionIdSpy).toBeCalledTimes(1);
    expect(patchPaymentRecordSpy).toBeCalledTimes(0);
    expect(findByTransactionIdSpy.mock.calls[0][0]).toMatchInlineSnapshot(`
      {
        "headers": {
          "channel": "BATCH",
          "correlation-id": "a580305a-dae9-46a4-a420-ddee91054d7a",
          "user-id": "CPS_EVENT",
        },
        "transactionId": "68B68A05E2B9C9971C0A",
      }
    `);
  });

  it("should only call payment apis with the correct parameters to update ERROR status as FAILED", async () => {
    const mockBody = {
      amount: 1870,
      reference: "HRTAB12CD34",
      status: "ERROR",
      transactionId: "68B68A05E2B9C9971C0A",
    };
    sqsRecord.body = JSON.stringify(mockBody);
    await processRecord(sqsRecord);
    expect(findByTransactionIdSpy).toBeCalledTimes(1);
    expect(patchPaymentRecordSpy).toBeCalledTimes(1);
    expect(findByTransactionIdSpy.mock.calls[0][0]).toMatchInlineSnapshot(`
      {
        "headers": {
          "channel": "BATCH",
          "correlation-id": "a580305a-dae9-46a4-a420-ddee91054d7a",
          "user-id": "CPS_EVENT",
        },
        "transactionId": "68B68A05E2B9C9971C0A",
      }
    `);
    expect(patchPaymentRecordSpy.mock.calls[0][0]).toMatchInlineSnapshot(`
      {
        "data": {
          "status": "FAILED",
        },
        "headers": {
          "channel": "BATCH",
          "correlation-id": "a580305a-dae9-46a4-a420-ddee91054d7a",
          "user-id": "CPS_EVENT",
        },
        "paymentId": "ac1bed7d-8597-156c-8185-97562f3c0000",
      }
    `);
  });

  it("should only call payment apis with the correct parameters to update CANCELLED status as FAILED", async () => {
    const mockBody = {
      amount: 1870,
      reference: "HRTAB12CD34",
      status: "CANCELLED",
      transactionId: "68B68A05E2B9C9971C0A",
    };
    sqsRecord.body = JSON.stringify(mockBody);
    await processRecord(sqsRecord);
    expect(findByTransactionIdSpy).toBeCalledTimes(1);
    expect(patchPaymentRecordSpy).toBeCalledTimes(1);
    expect(findByTransactionIdSpy.mock.calls[0][0]).toMatchInlineSnapshot(`
      {
        "headers": {
          "channel": "BATCH",
          "correlation-id": "a580305a-dae9-46a4-a420-ddee91054d7a",
          "user-id": "CPS_EVENT",
        },
        "transactionId": "68B68A05E2B9C9971C0A",
      }
    `);
    expect(patchPaymentRecordSpy.mock.calls[0][0]).toMatchInlineSnapshot(`
      {
        "data": {
          "status": "FAILED",
        },
        "headers": {
          "channel": "BATCH",
          "correlation-id": "a580305a-dae9-46a4-a420-ddee91054d7a",
          "user-id": "CPS_EVENT",
        },
        "paymentId": "ac1bed7d-8597-156c-8185-97562f3c0000",
      }
    `);
  });

  it.each(["ERROR_PAYMENT_NOT_FINISHED"])(
    "should not call payment apis with info log",
    async (cpsStatus: string) => {
      const mockBody = {
        amount: 1870,
        reference: "HRTAB12CD34",
        status: cpsStatus,
        transactionId: "68B68A05E2B9C9971C0A",
      };
      sqsRecord.body = JSON.stringify(mockBody);

      await expect(processRecord(sqsRecord)).resolves.not.toThrowError();
      expect(findByTransactionIdSpy).toBeCalledTimes(0);
      expect(patchPaymentRecordSpy).toBeCalledTimes(0);
    },
  );

  it.each([
    "FAILED_PAYMENT_CANCELLED_BY_USER",
    "FAILED_PAYMENT_METHOD_REJECTED",
    "FAILED_PAYMENT_EXPIRED",
  ])(
    "should only call payment apis with the correct parameters to update %p status as FAILED",
    async (cpsStatus: string) => {
      const mockBody = {
        amount: 1870,
        reference: "HRTAB12CD34",
        status: cpsStatus,
        transactionId: "68B68A05E2B9C9971C0A",
      };
      sqsRecord.body = JSON.stringify(mockBody);
      await processRecord(sqsRecord);
      expect(findByTransactionIdSpy).toBeCalledTimes(1);
      expect(patchPaymentRecordSpy).toBeCalledTimes(1);
      expect(findByTransactionIdSpy.mock.calls[0][0]).toMatchInlineSnapshot(`
              {
                "headers": {
                  "channel": "BATCH",
                  "correlation-id": "a580305a-dae9-46a4-a420-ddee91054d7a",
                  "user-id": "CPS_EVENT",
                },
                "transactionId": "68B68A05E2B9C9971C0A",
              }
          `);
      expect(patchPaymentRecordSpy.mock.calls[0][0]).toMatchInlineSnapshot(`
              {
                "data": {
                  "status": "FAILED",
                },
                "headers": {
                  "channel": "BATCH",
                  "correlation-id": "a580305a-dae9-46a4-a420-ddee91054d7a",
                  "user-id": "CPS_EVENT",
                },
                "paymentId": "ac1bed7d-8597-156c-8185-97562f3c0000",
              }
          `);
    },
  );

  it("should throw an error when sqs record body is empty", async () => {
    sqsRecord.body = JSON.stringify({});
    await expect(processRecord(sqsRecord)).rejects.toThrow(
      new Error(
        "Invalid record body, must contain amount, reference, transactionId and status",
      ),
    );
    expect(findByTransactionIdSpy).toBeCalledTimes(0);
    expect(patchPaymentRecordSpy).toBeCalledTimes(0);
  });

  it("should throw an error when sqs record body is unable to parse", async () => {
    sqsRecord.body = "This string unable to parse as object";
    await expect(processRecord(sqsRecord)).rejects.toThrow(
      new Error("Unexpected token T in JSON at position 0"),
    );
    expect(findByTransactionIdSpy).toBeCalledTimes(0);
    expect(patchPaymentRecordSpy).toBeCalledTimes(0);
  });

  it("should throw an error when status is ignored", async () => {
    sqsRecord.body = JSON.stringify({
      amount: 1870,
      reference: "HRTAB12CD34",
      status: "STATUS_IGNORED",
      transactionId: "68B68A05E2B9C9971C0A",
    });
    await expect(processRecord(sqsRecord)).rejects.toThrow(
      new Error(
        "No payment status update is needed, received [status: STATUS_IGNORED], only SUCCESS,FAILED_PAYMENT_CANCELLED_BY_USER,CANCELLED,FAILED_PAYMENT_METHOD_REJECTED,FAILED_PAYMENT_EXPIRED,ERROR will be processed",
      ),
    );
    expect(findByTransactionIdSpy).toBeCalledTimes(0);
    expect(patchPaymentRecordSpy).toBeCalledTimes(0);
  });

  it("should throw an error when status is in lower case", async () => {
    sqsRecord.body = JSON.stringify({
      amount: 1870,
      reference: "HRTAB12CD34",
      status: "success",
      transactionId: "68B68A05E2B9C9971C0A",
    });
    await expect(processRecord(sqsRecord)).rejects.toThrow(
      new Error(
        "No payment status update is needed, received [status: success], only SUCCESS,FAILED_PAYMENT_CANCELLED_BY_USER,CANCELLED,FAILED_PAYMENT_METHOD_REJECTED,FAILED_PAYMENT_EXPIRED,ERROR will be processed",
      ),
    );
    expect(findByTransactionIdSpy).toBeCalledTimes(0);
    expect(patchPaymentRecordSpy).toBeCalledTimes(0);
  });

  it("should throw an error when status is missing", async () => {
    sqsRecord.body = JSON.stringify({
      amount: 1870,
      reference: "HRTAB12CD34",
      transactionId: "68B68A05E2B9C9971C0A",
    });
    await expect(processRecord(sqsRecord)).rejects.toThrow(
      new Error(
        "Invalid record body, must contain amount, reference, transactionId and status",
      ),
    );
    expect(findByTransactionIdSpy).toBeCalledTimes(0);
    expect(patchPaymentRecordSpy).toBeCalledTimes(0);
  });

  it("should throw an error when amount is missing", async () => {
    sqsRecord.body = JSON.stringify({
      reference: "HRTAB12CD34",
      status: "SUCCESS",
      transactionId: "68B68A05E2B9C9971C0A",
    });
    await expect(processRecord(sqsRecord)).rejects.toThrow(
      new Error(
        "Invalid record body, must contain amount, reference, transactionId and status",
      ),
    );
    expect(findByTransactionIdSpy).toBeCalledTimes(0);
    expect(patchPaymentRecordSpy).toBeCalledTimes(0);
  });

  it("should throw an error when reference is missing", async () => {
    sqsRecord.body = JSON.stringify({
      amount: 1870,
      status: "SUCCESS",
      transactionId: "68B68A05E2B9C9971C0A",
    });
    await expect(processRecord(sqsRecord)).rejects.toThrow(
      new Error(
        "Invalid record body, must contain amount, reference, transactionId and status",
      ),
    );
    expect(findByTransactionIdSpy).toBeCalledTimes(0);
    expect(patchPaymentRecordSpy).toBeCalledTimes(0);
  });

  it("should throw an error when transactionId is missing", async () => {
    sqsRecord.body = JSON.stringify({
      amount: 1870,
      reference: "HRTAB12CD34",
      status: "SUCCESS",
    });
    await expect(processRecord(sqsRecord)).rejects.toThrow(
      new Error(
        "Invalid record body, must contain amount, reference, transactionId and status",
      ),
    );
    expect(findByTransactionIdSpy).toBeCalledTimes(0);
    expect(patchPaymentRecordSpy).toBeCalledTimes(0);
  });
});
