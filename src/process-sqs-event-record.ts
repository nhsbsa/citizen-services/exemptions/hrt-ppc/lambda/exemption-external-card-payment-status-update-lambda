import {
  loggerWithContext,
  setCorrelationId,
} from "@nhsbsa/hrt-ppc-npm-logging";
import { SQSRecord } from "aws-lambda";
import { randomUUID } from "crypto";
import { findByTransactionId, patchPaymentRecord } from "./api-utils";

// CPS payment status
const CpsPaymentStatus = {
  SUCCESS: "SUCCESS",
  FAILED_PAYMENT_CANCELLED_BY_USER: "FAILED_PAYMENT_CANCELLED_BY_USER",
  CANCELLED: "CANCELLED",
  FAILED_PAYMENT_METHOD_REJECTED: "FAILED_PAYMENT_METHOD_REJECTED",
  FAILED_PAYMENT_EXPIRED: "FAILED_PAYMENT_EXPIRED",
  ERROR: "ERROR",
} as const;
const CpsPaymentStatusExcludedError = {
  ERROR_PAYMENT_NOT_FINISHED: "ERROR_PAYMENT_NOT_FINISHED",
} as const;
// Exemption payment service status
const ExemptionPaymentStatus = {
  SUCCESS: "SUCCESS",
  FAILED: "FAILED",
} as const;

function validRecordbody(record: SQSRecord) {
  try {
    loggerWithContext().info(`Validating record body`);
    const { amount, reference, transactionId, status } = JSON.parse(
      record.body,
    );

    loggerWithContext().info(
      `Received [amount: ${amount}, reference: ${reference}, transactionId: ${transactionId}, status: ${status} ]`,
    );

    if (!amount || !reference || !transactionId || !status) {
      const message = `Invalid record body, must contain amount, reference, transactionId and status`;
      throw new Error(message);
    }
    if (!Object.values(CpsPaymentStatus).includes(status)) {
      if (Object.values(CpsPaymentStatusExcludedError).includes(status)) {
        const message = `No payment status update is needed, received [status: ${status}]`;
        loggerWithContext().info(message);
        return;
      } else {
        const message = `No payment status update is needed, received [status: ${status}], only ${Object.values(
          CpsPaymentStatus,
        )} will be processed`;
        throw new Error(message);
      }
    }
    return {
      amount,
      reference,
      transactionId,
      status,
    };
  } catch (err) {
    throw err;
  }
}

export async function processRecord(record: SQSRecord) {
  const correlationId = randomUUID();
  setCorrelationId(correlationId);
  loggerWithContext().info(`Processing event [messageId: ${record.messageId}]`);
  const recordBody = validRecordbody(record);

  if (recordBody) {
    const { amount, transactionId, status } = recordBody;

    const headers = {
      channel: "BATCH",
      "user-id": "CPS_EVENT",
      "correlation-id": correlationId,
    };

    try {
      const { id: paymentId } = await findByTransactionId({
        transactionId,
        headers,
      });
      if (!paymentId) {
        const message = `No payment record found [transactionId: ${transactionId}]`;
        loggerWithContext().info(message);
        throw new Error(message);
      }

      const statusToPatch =
        status === CpsPaymentStatus.SUCCESS
          ? ExemptionPaymentStatus.SUCCESS
          : ExemptionPaymentStatus.FAILED;

      loggerWithContext().info(
        `Patching payment record [paymentId: ${paymentId}, amount: ${amount}, status: ${statusToPatch}]`,
      );

      await patchPaymentRecord({
        paymentId,
        headers,
        data: {
          status: statusToPatch,
        },
      });

      loggerWithContext().info(
        `Records updated successfully [paymentId: ${paymentId}]`,
      );
    } catch (error) {
      loggerWithContext().info(
        `Payment record has not been updated [transactionId: ${transactionId}]`,
      );
      throw error;
    }
  }
}
