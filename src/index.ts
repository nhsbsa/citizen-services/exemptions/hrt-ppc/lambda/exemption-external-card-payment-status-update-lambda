import { SQSEvent } from "aws-lambda";
import { logger } from "@nhsbsa/hrt-ppc-npm-logging";
import { processRecord } from "./process-sqs-event-record";

export async function handler(event: SQSEvent) {
  try {
    logger.info("exemption-external-card-payment-status-update-lambda");
    const { Records: records } = event;

    logger.info(`Events received count [${records.length}]`);

    const results = await Promise.allSettled(
      records.map((record) => processRecord(record)),
    );
    const errors = results.filter(
      (result): result is PromiseRejectedResult => result.status === "rejected",
    );
    if (errors.length > 0) {
      errors.map((error) => {
        logger.error(`${error.reason?.message}: ${error.reason?.stack}`);
      });
      throw new Error(
        `[${errors.length}] out of total [${records.length}] records failed to process`,
      );
    }
    return "Processing finished";
  } catch (err) {
    logger.error({ message: "All records in this batch will be retried" });
    throw err;
  }
}
