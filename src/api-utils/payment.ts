import { PaymentApi } from "@nhsbsa/health-charge-exemption-npm-utils-rest";

const paymentApi = new PaymentApi();

export async function findByTransactionId({ transactionId, headers }) {
  return paymentApi.makeRequest({
    method: "GET",
    url: "/v1/payments/search/findByTransactionId",
    params: {
      transactionId,
    },
    headers,
    responseType: "json",
  });
}

export async function patchPaymentRecord({ paymentId, headers, data }) {
  await paymentApi.makeRequest({
    method: "PATCH",
    url: "/v1/payments/" + paymentId,
    headers,
    data,
    responseType: "json",
  });
}
