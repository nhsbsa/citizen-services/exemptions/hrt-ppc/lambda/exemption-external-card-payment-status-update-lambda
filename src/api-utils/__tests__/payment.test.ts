import { findByTransactionId, patchPaymentRecord } from "../payment";
import { PaymentApi } from "@nhsbsa/health-charge-exemption-npm-utils-rest";
import { mockPaymentRecord } from "../../__mocks__/mockData";

let paymentApiSpy: jest.SpyInstance;

const transactionId = "transactionId";
const paymentId = "paymentId";

const headers = {
  header: "value",
};
const data = "data";

beforeEach(() => {
  paymentApiSpy = jest
    .spyOn(PaymentApi.prototype, "makeRequest")
    .mockResolvedValue({});
});
afterEach(() => {
  jest.resetAllMocks();
});

describe("payment", () => {
  describe("findByTransactionId()", () => {
    it("should return the response successfuly", async () => {
      paymentApiSpy.mockResolvedValue(mockPaymentRecord);
      const response = await findByTransactionId({ transactionId, headers });
      expect(response).toBe(mockPaymentRecord);
    });

    it("should call the payment api with correct parameters", async () => {
      await findByTransactionId({ transactionId, headers });
      expect(paymentApiSpy).toHaveBeenCalledTimes(1);
      expect(paymentApiSpy.mock.calls).toMatchInlineSnapshot(`
        [
          [
            {
              "headers": {
                "header": "value",
              },
              "method": "GET",
              "params": {
                "transactionId": "transactionId",
              },
              "responseType": "json",
              "url": "/v1/payments/search/findByTransactionId",
            },
          ],
        ]
      `);
    });
  });

  describe("patchPaymentRecord()", () => {
    it("should not throw any error", async () => {
      paymentApiSpy = jest
        .spyOn(PaymentApi.prototype, "makeRequest")
        .mockResolvedValue({});
      expect(() =>
        patchPaymentRecord({ paymentId, headers, data }),
      ).not.toThrow();
    });

    it("should call the payment api with correct parameters", async () => {
      await patchPaymentRecord({ paymentId, headers, data });
      expect(paymentApiSpy).toHaveBeenCalledTimes(1);
      expect(paymentApiSpy.mock.calls).toMatchInlineSnapshot(`
        [
          [
            {
              "data": "data",
              "headers": {
                "header": "value",
              },
              "method": "PATCH",
              "responseType": "json",
              "url": "/v1/payments/paymentId",
            },
          ],
        ]
      `);
    });
  });
});
