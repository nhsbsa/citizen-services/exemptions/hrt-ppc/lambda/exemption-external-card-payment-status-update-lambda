export const mockSQSRecord = {
  messageId: "059f36b4-87a3-44ab-83d2-661975830a7d",
  receiptHandle: "AQEBwJnKyrHigUMZj6rYigCgxlaS3SLy0a",
  body: '{"amount":1870,"reference":"HRTAB12CD34","status":"SUCCESS","transactionId":"68B68A05E2B9C9971C0A"}',
  attributes: {
    ApproximateReceiveCount: "1",
    SentTimestamp: "1545082649183",
    SenderId: "AIDAIENQZJOLO23YVJ4VO",
    ApproximateFirstReceiveTimestamp: "1545082649185",
  },
  messageAttributes: {},
  md5OfBody: "098f6bcd4621d373cade4e832627b4f6",
  eventSource: "aws:sqs",
  eventSourceARN: "arn:aws:sqs:us-east-2:123456789012:my-queue",
  awsRegion: "us-east-2",
};

export const mockPaymentRecord = {
  id: "ac1bed7d-8597-156c-8185-97562f3c0000",
  certificateId: "e7fffacb-4575-47ba-b998-e0169945cdd5",
  method: "PREPAID",
  amount: 1870,
  date: "2022-02-01",
  status: "PENDING",
  transactionId: "68B68A05E2B9C9971C0A",
  _meta: {
    channel: "ONLINE",
    createdTimestamp: "2023-01-09T16:21:25.945164",
    createdBy: "ONLINE",
    updatedTimestamp: "2023-01-12T18:41:32.964458",
    updatedBy: "ONLINE",
  },
  _links: {
    self: {
      href: "http://localhost:8110/v1/payments/ac1bed7d-8597-156c-8185-97562f3c0000",
    },
    payment: {
      href: "http://localhost:8110/v1/payments/ac1bed7d-8597-156c-8185-97562f3c0000",
    },
  },
};
