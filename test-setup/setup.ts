process.env.paymentApiAuthKey = "paymentApiAuthKey";
process.env.paymentApiBaseUrl = "paymentApiBaseUrl";
process.env.outBoundApiTimeout = "10000";
process.env.certificateApiAuthKey = "certificateApiAuthKey";
process.env.certificateApiBaseUrl = "certificateApiBaseUrl";
process.env.logApiRequestAndResponse = "enable";
